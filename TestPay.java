package com.basic.day5;

public class TestPay {
	public static void paymentMode(Payment payment) {
		payment.pay();
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Payment p1 = new CreditCard();
		paymentMode(p1);
		p1 = new Paypal();
		paymentMode(p1);
		
	}

}
